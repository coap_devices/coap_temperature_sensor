#include <OneWire.h> 
#include <DallasTemperature.h>
#include <WiFi.h>
#include <WiFiUdp.h>
#include <coap.h>

#define ONE_WIRE_BUS 32         /* pin 32 used for 1-wire bus (other are possible) */
#define uS_TO_S_FACTOR 1000000  /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP  5        /* Time ESP32 will go to sleep (in seconds) */

/****************************************************************************

This is a demo application to implement a temperature sensor.
The device will connect to an WLAN access point, read temperature values of a Dallas temperature sensor
via 1-Wire bus and sends it to a pre-defined CoAP server
After sending data the sensor will go into deep sleep mode.

Modify the credentials and IP of the CoAP server for your own purpose!

coap://192.168.4.100/temp

****************************************************************************/

//WPA2 credentials
const char* ssid     = "myssid";
const char* password = "mypassword";

WiFiUDP udp;
Coap coap(udp);

// oneWire instance to communicate with other OneWire devices (independent of manifacturer)
OneWire oneWire(ONE_WIRE_BUS); 

// sensor instance with reference to 1-wire bus
DallasTemperature sensors(&oneWire);

float temperature;
char temp_array[20];            /*coap payload*/

/**************************************************************************************************************/

void readtemp(){
 sensors.requestTemperatures();               /* Send the command to get temperature readings */
 temperature = sensors.getTempCByIndex(0);
}

void print_wakeup_reason(){
  esp_sleep_wakeup_cause_t wakeup_reason;

  wakeup_reason = esp_sleep_get_wakeup_cause();

  switch(wakeup_reason)
  {
    case 1  : Serial.println("Wakeup caused by external signal using RTC_IO"); break;
    case 2  : Serial.println("Wakeup caused by external signal using RTC_CNTL"); break;
    case 3  : Serial.println("Wakeup caused by timer"); break;
    case 4  : Serial.println("Wakeup caused by touchpad"); break;
    case 5  : Serial.println("Wakeup caused by ULP program"); break;
    default : Serial.println("Wakeup was not caused by deep sleep"); break;
  }
}

/**************************************************************************************************************/

void setup(void) 
{ 
 // start serial port 
 Serial.begin(9600); 
 
 print_wakeup_reason();

 WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // timer causes the ESP32 to wake up
  // interval is set to 5 seconds
  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
  Serial.println("Setup ESP32 to sleep for every " + String(TIME_TO_SLEEP) +" Seconds");

  // read temperature from sensor
  sensors.begin(); 
  readtemp();

  coap.start();

  // convert temperature value to a representation with one decimal digit
  sprintf(temp_array, "%.1f", temperature);
  Serial.println(temp_array);
  
  // send temperature value to pre-defined CoAP server via PUT message
  coap.put(IPAddress(192,168,4,100), 5683, "temp", temp_array);
  coap.loop();

  //deep sleep
  esp_deep_sleep_start();
} 

void loop(void) 
{ 
  
} 
